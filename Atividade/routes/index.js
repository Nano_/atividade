var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var db = require("../db");

  db.findProdutos(function(err, documentos){
    if(err)
      res.render('index', { title: 'Erro', produtos: [] });
    else
      res.render('index', { title: 'Express', produtos: documentos });
  });

});

module.exports = router;
